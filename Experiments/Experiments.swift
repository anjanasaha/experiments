import Firebase

public typealias RemoteConfigFetchResultBlock = (RemoteConfigFetchResult) -> Void

public enum FeatureName : String {
    case isTabBarEnabled = "is_tab_bar_enabled"
    case isAutomaticThemingOn = "is_automatic_theming_on"
    case shouldShareListViaSMS = "should_share_list_via_sms"
    case faqLink = "faq_link"
    case termsAndConditionsLink = "terms_and_conditions_link"
    case privacyPolicyLink = "privacy_policy_link"
    case licensesLink = "licenses_link"
    case allowChangingPhoneNumber = "allow_changing_phone_number"
    case allowChangingEmail = "allow_changing_email"
    case contactEmail = "contact_email"
    case website = "website"
}

public enum RemoteConfigFetchResult {
    case success
    case failure
}

public enum RemoteConfigLoadingStyle {
    case applyNow
    case onNextBoot
}

public protocol Experimenting : class {
    func fetch(loadingStyle: RemoteConfigLoadingStyle, completion: RemoteConfigFetchResultBlock?)
    func isFeatureEnabled(name: FeatureName, defaultValue: Bool) -> Bool
    func stringValue(name: FeatureName, defaultValue: String) -> String
}

public final class RemoteConfiguration: Experimenting {

    private var remoteConfig: RemoteConfig
    
    public func stringValue(name: FeatureName, defaultValue: String) -> String {
        let value = remoteConfig[name.rawValue]
        if let valueString = value.stringValue {
            return valueString
        } else {
            return defaultValue
        }
    }

    public func isFeatureEnabled(name : FeatureName, defaultValue: Bool = false) -> Bool {
        let featureEnabled = remoteConfig[name.rawValue]
        if featureEnabled.boolValue {
            return true
        } else {
            return defaultValue
        }
    }
    
    public init(storeType: ExperimentsSourceType) {
        self.remoteConfig =  RemoteConfig.remoteConfig()
    }
    
    public func fetch(loadingStyle: RemoteConfigLoadingStyle, completion: RemoteConfigFetchResultBlock?) {

        /// TODO - Implement
        switch loadingStyle {
            case .onNextBoot:
                remoteConfig.activate { (_, error) in
                    guard error != nil else {
                        completion?(.success)
                        return
                    }
                    completion?(.failure)
                }
                remoteConfig.fetch(completionHandler: nil)
            default:
                
                let settings = RemoteConfigSettings()
                settings.minimumFetchInterval = 0
                remoteConfig.configSettings = settings

                remoteConfig.fetchAndActivate { (status, error) in
                    
                    switch status {
                        case .successUsingPreFetchedData:
                            print("Success using pre fetched data")
                        case .successFetchedFromRemote:
                            print("Success fetched from remote")
                        case .error:
                            print("Error fetching and activating")
                        default:
                            break
                    }
                }
        }
        
    }
}

public enum ExperimentsSourceType {
    case firebase
}
