Pod::Spec.new do |s|
  s.name             = 'Experiments'
  s.version          = '0.1.0'
  s.summary          = 'This pod allows A/B Testing.'

  s.description      = <<-DESC
  This pod facilitates AB Testing. Currently, it is powered by Firebase only.
                       DESC
  s.homepage         = 'https://anjanasaha@bitbucket.org/anjanasaha/experiments.git'
  s.license          = { :type => 'MIT', :file => 'LICENSE.txt' }
  s.author           = { 'anjanasaha' => 'anjanasaha26@gmail.com' }

  s.source           = { :git => 'https://anjanasaha@bitbucket.org/anjanasaha/experiments.git', :tag => 'v' + s.version.to_s }
  s.ios.deployment_target = '13.0'

  s.source_files = "Experiments/*.{swift}"
  s.swift_version = "5.0"
  s.static_framework = true

  s.frameworks = 'MapKit', 'Foundation', 'SystemConfiguration', 'CoreText', 'QuartzCore', 'Security', 'UIKit', 'Foundation', 'CoreGraphics','CoreTelephony', 'FirebaseCore', 'FirebaseRemoteConfig', 'FirebaseInstanceID', 'FirebaseAnalytics', 'FirebaseABTesting', 'FirebaseCoreDiagnostics', 'FirebaseNanoPB'
  s.libraries = 'c++', 'sqlite3', 'z'

  s.dependency 'Firebase'
  s.dependency 'Firebase/Core'
  s.dependency 'Firebase/RemoteConfig'
  s.dependency 'CommonLib', '0.1.0'

  s.pod_target_xcconfig = {
    'FRAMEWORK_SEARCH_PATHS' => '$(inherited) $(PODS_ROOT)/Firebase $(PODS_ROOT)/FirebaseCore/Frameworks $(PODS_ROOT)/FirebaseRemoteConfig/Frameworks $(PODS_ROOT)/FirebaseInstanceID/Frameworks $(PODS_ROOT)/FirebaseAnalytics/Frameworks $(PODS_ROOT)/FirebaseABTesting/Frameworks'
  }

  s.pod_target_xcconfig = {
    'OTHER_LDFLAGS' => '$(inherited) -ObjC'
  }
end